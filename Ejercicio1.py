# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-


# Función que ordena la tupla y que, luego,
# la cambia por una lista.
def cambiazo_tupla_a_lista(tupla):

    lista = []

    for i in range(len(tupla)):
        if i == 0:
            lista.append(tupla[len(tupla)-1])
        else:
            lista.append(tupla[(i-1)])

    # Tuple se utiliza para cambiar una tupla por una lista.
    return tuple(lista)


if __name__ == '__main__':

    tupla = (6, 16, 21, 31, 2, 6, 11, 6, 14)

    lista_cambiada = cambiazo_tupla_a_lista(tupla)
    print("\n")
    print("La tupla original es: ", tupla)
    print("La lista con los elementos ordenados de la tupla es: "
          , lista_cambiada)
    print("\n")
