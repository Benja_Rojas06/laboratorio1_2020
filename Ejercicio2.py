# !/usr/bin/env python3
# ! -*- coding:utf-8 -*-

import random
import os


# Función que verifica cual es el numero menor.
def numero_menor(matriz, x, y):

    numero_menor = 10

    for i in range(x):
        for j in range(y):
            # Comparación entre los números.
            if matriz[i][j] <= numero_menor:
                numero_menor = matriz[i][j]

    return numero_menor


# Función que suma las 5 primeras columnas.
def suma_columnas(matriz, x, y=5):

    suma_de_columnas = []

    # Contador con el cual se agregarán a la lista
    # "suma_de_filas" los elementos sumados.
    contador = 0

    # Se recorren las columnas a sumar.
    for i in range(x):
        for j in range(y):
            if(j<= 5):
                contador = contador + matriz[i][j]
        suma_de_columnas.append(contador)

        # Contador vuelve a 0 para sumar la siguiente columna.
        contador = 0

    return suma_de_columnas


# Función que retorna una lista con los números negativos
def numeros_negativos(matriz, x, y):

    numeros_negativos = []

    for i in range(x):
        for j in range(y):
            # Rango que va desde la cuarta hasta la novena fila.
            if (matriz[i][j] < 0 and j >= 4 and j <=9):
                numeros_negativos.append(matriz[i][j])

    return numeros_negativos


def menu():

    matriz = []
    i = 15
    j = 12

    rellenar_matriz(matriz, i, j)
    os.system("clear")
    print("\n")
    print("¿Qué desea realizar con la siguiente matriz?")
    print("\n")
    imprimir_matriz(matriz, i, j)

    print("\n")
    print("1.- Obtener el menor número de la matriz")
    print("2.- Sumar los números de las cinco primeras columnas de cada fila"
          " de la matriz")
    print("3.- Mostrar todos los números negativos presentes desde la quinta"
          " a la novena columna")
    print("***Si desea salir, presione cualquier otra tecla***")
    print("\n")

    while True:
        opcion = input("Ingrese una opción: ")

        # Opciones del menú.
        if opcion == "1":

            menor = numero_menor(matriz, i, j)
            print("El menor número de la matriz es: ", menor)

        elif opcion == "2":

            suma = suma_columnas(matriz, i)
            print("La suma de los números de las cinco primeras columnas de"
                  " cada fila es: ", suma)

        elif opcion == "3":

            negativos = numeros_negativos(matriz, i, j)
            print("Los números negativos presentes desde la quinta a la novena"
                  " columna son: ", negativos)

        else:
            quit()


# Función que imprime la matriz
def imprimir_matriz(matriz, x, y):

    # Detalles estéticos.
    for j in range(y):
        print(j+1, end='\t')
    print("")

    for i in range(x):
        for j in range(y):
            print("[", matriz[i][j], "]", end='\t')
        print(i+1)


# Función que rellena la matriz con números desde
# el -10 al 10.
def rellenar_matriz(matriz, x, y):

    # Generación matriz vacía.
    for i in range(x):
        matriz.append([0]*y)

    # Generación matriz random.
    for i in range(x):
        for j in range(y):
            matriz[i][j] = random.randint(-10,10)


if __name__ == '__main__':

    menu()
